#!/bin/bash

#===========================================================
#   ___                               __        __   _
#  |_ _|_ __   __ _  __ _  __ _  ___  \ \      / /__| |__
#   | || '_ \ / _` |/ _` |/ _` |/ _ \  \ \ /\ / / _ \ '_ \
#   | || | | | (_| | (_| | (_| |  __/   \ V  V /  __/ |_) |
#  |___|_| |_|\__, |\__,_|\__, |\___|    \_/\_/ \___|_.__/
#             |___/       |___/
#===========================================================

#-----------------------------------------------------------
#   _          _
#  | |__   ___| |_ __
#  | '_ \ / _ \ | '_ \
#  | | | |  __/ | |_) |
#  |_| |_|\___|_| .__/
#               |_|
#-----------------------------------------------------------
#
#    Usage:
#        ./ingage.sh [command] [options]
#
#    Example use:
#        ./ingage.sh run --dev
#
#    Commands:
#        run
#                       Run the part or all of the application.
#
#        build
#                       Build part or all of the application
#
#        stop
#                        Bring the application down
#
#        down [deprecated]
#                       Bring the application down
#
#    Options:
#        -D
#        --daemon
#                       Run the application in the background
#
#        -l
#        --local
#                      Use the local development configuration
#
#        -b
#        --build
#                       Build the application before running
#
#        -h
#        --help
#                       Show the help message
#
#        -d [deprecated]
#        --dev [deprecated]
#        --development [deprecated]
#                       Use the development configuration
#
#        -s [deprecated]
#        --staging [deprecated]
#                       Use the staging configuration
#
#        -p [deprecated]
#        --prod [deprecated]
#        --production [deprecated]
#                       Use the production configuration
#

#-----------------------------------------------------------
#           _
#  ___  ___| |_ _   _ _ __
# / __|/ _ \ __| | | | '_ \
# \__ \  __/ |_| |_| | |_) |
# |___/\___|\__|\__,_| .__/
#                   |_|
#-----------------------------------------------------------
 
# Whether or not to print the help dialog
show_help=false

# What environment we are running in
environment="production"

# Command arguments with a default value of nothing
env_args=
docker_args=
compose_file_args=

# Loop through each argument and set appropriate values
for arg in "$@"
do
    case $arg in
        "-D" |\
        "--daemon")
            docker_args="$DOCKER_ARGS -d"
        ;;

        "-l" |\
        "--local")
            compose_file_args="-f docker-compose.local.yml $DOCKER_ARGS"
        ;;

        "-d" |\
        "--dev" |\
        "--development")
            environment="development"
            env_args="$ENV_ARGS NODE_ENV=development"
        ;;

        "-s" |\
        "--staging")
            environment="staging"
            env_args="$ENV_ARGS NODE_ENV=staging"
        ;;

        "-p" |\
        "--prod" |\
        "--production")
            env_args="$ENV_ARGS NODE_ENV=production"
        ;;

        "-b" |\
        "--build")
            docker_args="--build $DOCKER_ARGS"
        ;;

        "-h" |\
        "--help" |\
        "help")
            show_help=true
        ;;
    esac
done

#-----------------------------------------------------------
#      __                  _   _
#     / _|_   _ _ __   ___| |_(_) ___  _ __  ___
#    | |_| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
#    |  _| |_| | | | | (__| |_| | (_) | | | \__ \
#    |_|  \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
#-----------------------------------------------------------

# Print help menu
function ingage_help {
    echo "
Ingage Web:
===========

    Usage:
        ./ingage.sh [command] [options]

    Example use:
        ./ingage.sh run --dev

    Commands:
        run
                       Run the part or all of the application.

        build
                       Build part or all of the application

        stop
                        Bring the application down

        down [deprecated]
                       Bring the application down

    Options:
        -D
        --daemon
                       Run the application in the background

        -l
        --local
                      Use the local development configuration

        -b
        --build
                       Build the application before running

        -h
        --help
                       Show the help message

        -d [deprecated]
        --dev [deprecated]
        --development [deprecated]
                       Use the development configuration

        -s [deprecated]
        --staging [deprecated]
                       Use the staging configuration

        -p [deprecated]
        --prod [deprecated]
        --production [deprecated]
                       Use the production configuration
    "
}



#-----------------------------------------------------------
#   _ __ _   _ _ __
#  | '__| | | | '_ \
#  | |  | |_| | | | |
#  |_|   \__,_|_| |_|
#-----------------------------------------------------------

# If we are trying to run the application
if [[ $show_help == true ]]; then
    ingage_help
elif [[ "$1" == "run" ]]; then
    docker-compose $compose_file_args up $docker_args
elif [[ "$1" == "build" ]]; then
    docker-compose $compose_file_args up $docker_args
elif [[ "$1" == "stop" ]]; then
    docker-compose $compose_file_args down
elif [[ "$1" == "down" ]]; then
    docker-compose $compose_file_args down
else
    echo "Unknown command: $@"
    echo "Run \"./ingage.sh --help\" for help."
fi
