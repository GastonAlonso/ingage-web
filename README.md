Ingage Web
==========

The composed stack for Ingage Web.

### Environment Configuration

Use these values in the `.env` file to enable a specific environment

| Environment Name |
|------------------|
| development      |
| staging          |
| production       |

### Development

1\. Clone this repository **recursively**

```shell
git clone --recursive https://bitbucket.org/scrollmotiongit/ingage-web.git

cd ingage-web
```

2\. Update any submodules that need it just in case

```shell
git submodule update --init
```

3\. Configure Redis connection options

```shell
# modify `.env` with the right values

# eg.
sed -i -e 's|=redis|=123\.456\.7\.89|g' .env
```

4\. Start the application

```shell
# using shell script
./ingage.sh run -d -b

# using docker-compose directly

# for the entire stack
docker-compose -f docker-compose.dev.yml up

# for just the api
docker-compose -f docker-compose.dev.yml up api-v1

# for the node app and api
docker-compose -f docker-compose.dev.yml up ingage-server

# for nginx, node app, and api
docker-compose -f docker-compose.dev.yml up nginx ingage-server
```

##### Ports

Services are exposed on the host machine during development, here are their mappings:

| Service  | Port |
|----------|------|
| NGINX    | 80   |
| Node App | 8082 |
| Node API | 8083 |

### Deployment

1\. Clone this repository

```shell
git clone --recursive https://bitbucket.org/scrollmotiongit/ingage-web.git

cd ingage-web
```

2\. Update any submodules that need it just in case

```shell
git submodule update --init
```

3\. Configure Redis connection options

```shell
# modify `ingage-web/ingage/server/.env` with the right values

# eg.
sed -i -e 's|=redis|=123\.456\.7\.89|g' ./ingage/server/.env
```

4\. Start the application

```shell
docker-compose up
```

##### Ports

The only entrypoint in production is port 80 through NGINX.

| Service | Port |
|---------|------|
| NGINX   | 80   |

### File Structure

```
ingage-web/
├── api/                       (all api processes)
│   ├── v1/                    (version 1 of the api)
│
├── ingage/                    (application server and client resources)
│   ├── client/                (client resources)
│   ├── server/                (server resources)
│
├── nginx/                     (nginx configuration)
│   ├── configs/               (individual site configuration)
│   │   ├── app.conf           (configuration file for the application server)
│   ├── Dockerfile             (dockerfile to build the nginx image)
│   ├── nginx.conf             (root nginx configuration file)
│
├── .dockerignore              (ensure docker ignores certain files)
├── .gitignore                 (ensure git ignores certain files)
├── .env                       (environment variable configuration)
├── .gitmodules                (configuration for submodules of the project)
├── docker-compose.dev.yml     (development compose file for the application)
├── docker-compose.staging.yml (staging compose file for the application)
├── docker-compose.yml         (production compose file for the application)
├── ingage.sh                  (helper script to launch the application)
├── README.md                  (this file)
```
